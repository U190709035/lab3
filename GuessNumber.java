import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) {
		Random randomNumber = new Random();
		Scanner scan = new Scanner(System.in);
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");

		int randomNum = randomNumber.nextInt(100);
		int enterNumber = scan.nextInt();

		int count = 0;

		while (enterNumber != -1) {
			count++;

			if (enterNumber > randomNum) {
				System.out.println("Sorry\n" + "Mine ise greater than your guess.");
				System.out.print("Type -1 to quit or guess another:");
				enterNumber = scan.nextInt();
			} else if (enterNumber < randomNum) {
				System.out.println("Sorry\n" + "Mine ise less than your guess.");
				System.out.print("Type -1 to quit or guess another:");
				enterNumber = scan.nextInt();

			} else if (enterNumber == randomNum) {
				System.out.println("Congratulations! You won after " +  count + " attempts!" );
				break;
			}



		}
	}
}

